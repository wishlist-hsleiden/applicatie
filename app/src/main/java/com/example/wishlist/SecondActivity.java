package com.example.wishlist;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.wishlist.database.AppDatabase;
import com.example.wishlist.database.Wishlist;
import com.example.wishlist.database.WishlistDoa;

import java.util.ArrayList;
import java.util.List;

public class SecondActivity<image> extends AppCompatActivity {
    private Toolbar mTopToolbar;

    RecyclerView recyclerView;

    private CardView cardview;
    private TextView title;
    private TextView description;
    private ImageView buttonTo;
    private ImageButton addWishlist;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);


        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Wishlist");

        setSupportActionBar(mTopToolbar);

        cardview = findViewById(R.id.card_view);
        title = findViewById(R.id.Title);
        description = findViewById(R.id.Description);
        buttonTo = findViewById(R.id.To);
        addWishlist = findViewById(R.id.newWishlist);

        addWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(SecondActivity.this, AddWishlistActivity.class), 100);
            }
        });

        getWishlists();

        }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == 100) {
            Log.d("BACK", "BACK");
            getWishlists();
        }
    }

    public void getWishlists() {
        AppDatabase database = AppDatabase.getInstance(SecondActivity.this);

        WishlistDoa wishlistDoa = database.wishlistDoa();
        List<Wishlist> wishlists = wishlistDoa.getWishlists();

        List<WishlistCard> wishlistCards = new ArrayList<>();

        for(int i = 0; i < wishlists.size(); i++) {
            wishlistCards.add(
                    new WishlistCard(
                            wishlists.get(i).getId(),
                            wishlists.get(i).getTitle(),
                            wishlists.get(i).getDescription()
                    ));
        }

        WishlistAdapter adapter = new WishlistAdapter(this, wishlistCards);

        recyclerView.setAdapter(adapter);
    }
}
