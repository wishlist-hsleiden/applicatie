package com.example.wishlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.wishlist.database.AppDatabase;
import com.example.wishlist.database.Product;
import com.example.wishlist.database.ProductDoa;
import com.example.wishlist.database.Wishlist;
import com.example.wishlist.database.WishlistDoa;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;
import java.util.List;

public class WishlistActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    private int value;

    FloatingActionMenu materialDesignFAM;
    FloatingActionButton floatingActionButton1, floatingActionButton2;

    private CardView cardview;
    private TextView title;
    private TextView description;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist);

        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Wishlist - Product");

        Bundle b = getIntent().getExtras();
        value = -1; // or other values
        if(b != null)
            value = b.getInt("id");

        AppDatabase database = AppDatabase.getInstance(WishlistActivity.this);

        final WishlistDoa wishlistDoa = database.wishlistDoa();
        final Wishlist wishlist = wishlistDoa.getWishlistById(value);

        recyclerView = (RecyclerView) findViewById(R.id.productView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        cardview = findViewById(R.id.card_view);
        title = findViewById(R.id.Title);
        title.setText(wishlist.getTitle());
        description = findViewById(R.id.Description);
        description.setText(wishlist.getDescription());

        materialDesignFAM = (FloatingActionMenu) findViewById(R.id.material_design_android_floating_action_menu);
        floatingActionButton1 = (FloatingActionButton) findViewById(R.id.material_design_floating_action_menu_item1);
        floatingActionButton2 = (FloatingActionButton) findViewById(R.id.material_design_floating_action_menu_item2);

        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(WishlistActivity.this,AddProductActivity.class);
                Bundle b = new Bundle();
                b.putInt("id", value); //Your id
                intent.putExtras(b); //Put your id to your next Intent
                startActivityForResult(intent, 200);
            }
        });
        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                wishlistDoa.delete(wishlist);

                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        getProducts();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == 200) {
            Log.d("BACK", "BACK");
            getProducts();
        }
    }

    public void getProducts(){
        AppDatabase database = AppDatabase.getInstance(WishlistActivity.this);

        ProductDoa productDoa = database.productDoa();
        List<Product> products = productDoa.getProductsById(value);

        List<ProductCard> productCards = new ArrayList<>();

        for(int i = 0; i < products.size(); i++) {
            productCards.add(
                    new ProductCard(
                            products.get(i).getId(),
                            products.get(i).getTitle(),
                            products.get(i).getDescription(),
                            products.get(i).getPrice()
                    ));
        }

        ProductAdapter adapter = new ProductAdapter(this, productCards);

        recyclerView.setAdapter(adapter);
    }
}
