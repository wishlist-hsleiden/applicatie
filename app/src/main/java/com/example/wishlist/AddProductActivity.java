package com.example.wishlist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.example.wishlist.database.AppDatabase;
import com.example.wishlist.database.Product;
import com.example.wishlist.database.ProductDoa;
import com.example.wishlist.database.WishlistDoa;

public class AddProductActivity extends AppCompatActivity {

    private EditText ProductName;
    private EditText ProductDescription;
    private EditText ProductPrice;
    private Button CreateProduct;

    public int value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        Bundle b = getIntent().getExtras();
        value = -1; // or other values
        if(b != null)
            value = b.getInt("id");

        ProductName = (EditText)findViewById(R.id.ProductInputName);
        ProductDescription = (EditText)findViewById(R.id.ProductInputDescription);
        ProductPrice = (EditText)findViewById(R.id.ProductInputPrice);
        CreateProduct = (Button)findViewById(R.id.btnCreateProduct);

        CreateProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = ProductName.getText().toString();
                String desc = ProductDescription.getText().toString();
                double price = Double.valueOf(ProductPrice.getText().toString());

                createProduct(value, title, desc, price);
            }
        });
    }

    public void createProduct(int id, String title, String desc, double price){
        AppDatabase database = AppDatabase.getInstance(AddProductActivity.this);

        ProductDoa productDoa = database.productDoa();

        Product product = new Product();
        product.setWishlistId(id);
        product.setTitle(title);
        product.setDescription(desc);
        product.setPrice(price);

        productDoa.insert(product);

        finish();
    }
}
