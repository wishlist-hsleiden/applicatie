package com.example.wishlist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.wishlist.database.AppDatabase;
import com.example.wishlist.database.Wishlist;
import com.example.wishlist.database.WishlistDoa;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EditText Name;
    private EditText Password;
    private TextView Info;
    private Button Login;
    private Button signUp;
    private int counter = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppDatabase database = AppDatabase.getInstance(MainActivity.this);

        WishlistDoa wishlistDoa = database.wishlistDoa();

//        Wishlist wishlist = new Wishlist();
//        wishlist.setUserId(1);
//        wishlist.setTitle("Titel 1");
//        wishlist.setDescription("DIT IS VERDOMME DE EERSTE WISHLIST");
//        wishlist.setShared(false);
//
//        wishlistDoa.insert(wishlist);

        List<Wishlist> wishlists = wishlistDoa.getWishlists();

        Name = (EditText)findViewById(R.id.etName);
        Password = (EditText)findViewById(R.id.etPassword);
        Info = (TextView)findViewById(R.id.tvInfo);
        Login = (Button)findViewById(R.id.btnLogin);
        signUp = (Button)findViewById(R.id.btnSignUp);

        Info.setText("Pogingen over: 5");

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate(Name.getText().toString(), Password.getText().toString());
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SignUp.class));
            }
        });
    }

    private void validate(final String userName, final String userPassword){

        if(userName.equalsIgnoreCase("jeroenRijsdijk") && userPassword.equals("RobbertWinkel")) {
            Intent intent = new Intent(MainActivity.this, SecondActivity.class);
            startActivity(intent);
            Toast.makeText(MainActivity.this, "Ingelogt", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(MainActivity.this, "email of wachtwoord onjuist", Toast.LENGTH_LONG).show();

            Info.setText("Pogingen over: " + String.valueOf(counter));

            if(counter == 0){
                Login.setEnabled(false);
            }
        }
    }
}
