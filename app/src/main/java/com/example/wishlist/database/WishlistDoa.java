package com.example.wishlist.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface WishlistDoa {

    @Query("SELECT * FROM wishlist")
    public List<Wishlist> getWishlists();

    @Query("SELECT * FROM wishlist WHERE id = :id")
    public Wishlist getWishlistById(int id);

    @Insert
    public void insert(Wishlist... items);
    @Update
    public void update(Wishlist... items);
    @Delete
    public void delete(Wishlist item);

    @Query("SELECT * FROM wishlist")
    List<Wishlist> loadAll();

}
