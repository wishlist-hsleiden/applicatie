package com.example.wishlist;

public class ProductCard {
    private int id;
    private String title;
    private String description;
    private double price;

    public ProductCard(int id, String title, String description, double price){
        this.id = id;
        this.title = title;
        this.description = description;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public double getPrice() {
        return price;
    }
}
