package com.example.wishlist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.wishlist.database.AppDatabase;
import com.example.wishlist.database.Product;
import com.example.wishlist.database.ProductDoa;
import com.example.wishlist.database.Wishlist;
import com.example.wishlist.database.WishlistDoa;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

public class ProductActivity extends AppCompatActivity {

    private int value;
    private Toolbar toolbar;

    FloatingActionMenu productMenu;
    FloatingActionButton deleteProductButton;

    private TextView title;
    private TextView description;
    private TextView price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Product");


        Bundle b = getIntent().getExtras();
        value = -1; // or other values
        if(b != null)
            value = b.getInt("product");

        AppDatabase database = AppDatabase.getInstance(ProductActivity.this);

        final ProductDoa productDoa = database.productDoa();
        final Product product = productDoa.getProductById(value);

        title = (TextView)findViewById(R.id.ProductCardName);
        title.setText(product.getTitle());
        description = (TextView)findViewById(R.id.ProductCardDescription);
        description.setText(product.getDescription());
        price = (TextView)findViewById(R.id.ProductCardPrice);
        price.setText(Double.toString(product.getPrice()));

        productMenu = (FloatingActionMenu) findViewById(R.id.DeleteProductActionButton);
        deleteProductButton = (FloatingActionButton) findViewById(R.id.deleteProductButton);


        deleteProductButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                productDoa.delete(product);

                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });



    }
}
