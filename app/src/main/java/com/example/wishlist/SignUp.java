package com.example.wishlist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SignUp extends AppCompatActivity {

    private TextView backHome;
    private Button SignUp;
    private EditText name;
    private EditText email;
    private EditText password;
    private EditText confirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        backHome = (TextView)findViewById(R.id.backToHome);
        SignUp = (Button)findViewById(R.id.btnSignUp);
        name = (EditText)findViewById(R.id.etName);
        email = (EditText)findViewById(R.id.email);
        password = (EditText)findViewById(R.id.pass);
        confirmPassword = (EditText)findViewById(R.id.confPass);

        backHome.setText(Html.fromHtml("<u>Terug naar login pagina</u>"));


        backHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignUp.this, MainActivity.class));
            }
        });

        SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate(name.getText().toString(), email.getText().toString(), password.getText().toString(), confirmPassword.getText().toString());
            }
        });
    }

    private void validate(final String userName, final String email, final String password, final String confirmPassword){

        Toast.makeText(SignUp.this, "" + "account aangemaakt", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(SignUp.this, SecondActivity.class);
        startActivity(intent);
    }

}
