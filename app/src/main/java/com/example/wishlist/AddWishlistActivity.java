package com.example.wishlist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import com.example.wishlist.database.AppDatabase;
import com.example.wishlist.database.Wishlist;
import com.example.wishlist.database.WishlistDoa;

public class AddWishlistActivity extends AppCompatActivity {

    private EditText WishlistName;
    private EditText WishlistDescription;
    private Switch WishlistShare;
    private Button CreateWishlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_wishlist);

        WishlistName = (EditText)findViewById(R.id.wishlistInputName);
        WishlistDescription = (EditText)findViewById(R.id.wishlistInputDescription);
        WishlistShare = (Switch) findViewById(R.id.wishlistShare);
        CreateWishlist = (Button) findViewById(R.id.btnCreateWishlist);

        CreateWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String title = WishlistName.getText().toString();
                String desc = WishlistDescription.getText().toString();
                boolean shared = WishlistShare.isChecked();

                createWishlist(title, desc, shared);
            }
        });

    }

    private void createWishlist(String title, String desc, boolean shared){

        AppDatabase database = AppDatabase.getInstance(AddWishlistActivity.this);

        WishlistDoa wishlistDoa = database.wishlistDoa();

        Wishlist wishlist = new Wishlist();
        wishlist.setUserId(1);
        wishlist.setTitle(title);
        wishlist.setDescription(desc);
        wishlist.setShared(shared);

        wishlistDoa.insert(wishlist);


        finish();
    }
}
