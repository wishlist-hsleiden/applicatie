package com.example.wishlist;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class NetworkManager {

    private static final String TAG = "NetworkManager";
    private static NetworkManager instance = null;
    private static final String prefixURL = "http://10.0.2.2:8000/api";

    public RequestQueue requestQueue;

    private NetworkManager(Context context){
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
    }

    public static synchronized NetworkManager getInstance(Context context) {
        if(instance == null){
            instance = new NetworkManager(context);
        }
        return instance;
    }

    public static synchronized NetworkManager getInstance() {
        if(null == instance){
            throw new IllegalStateException("call getInstance(...) first!");
        }
        return instance;
    }

    public void getRequest(String url, final VolleyCallBack callback){

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
            (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    callback.onSuccess(response);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    callback.onError(error);
                }
            });
        requestQueue.add(jsonObjectRequest);
    }

    public void postRequest(String url, JSONObject sendObj, final VolleyCallBack callback) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, sendObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                callback.onSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImUwNTMyNTVmOGUzMzlmZWY5NDhlMjc0MDJlNmM3ZjdlZTljMzdmODg2ODFiZDE2ZTQyNGQyYzE5OTIzNDMwOTE1OWU0MzUyNzU1Y2U3MzA0In0.eyJhdWQiOiIyIiwianRpIjoiZTA1MzI1NWY4ZTMzOWZlZjk0OGUyNzQwMmU2YzdmN2VlOWMzN2Y4ODY4MWJkMTZlNDI0ZDJjMTk5MjM0MzA5MTU5ZTQzNTI3NTVjZTczMDQiLCJpYXQiOjE1NjE2MzI0NjMsIm5iZiI6MTU2MTYzMjQ2MywiZXhwIjoxNTkzMjU0ODYzLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.nW3_qpzLpNK_8hF1D3KYnt8lTz4STRvPfpf2uKb497hGQh8By8rukWmICgmMtnF_ZkgwrZKLTGNF_yRWU6W4X-KrlDmpsRAYxLtG-08wmwF1kjh4OQ5pJPZoRm8LPsovgxyNYdNTckVSoV5uMBM8J5DH3HsIyTC64BG7bk_EuNKPI0Syi3we94ESnY2eXYrj_z_YpMBb5hYaZ1R0-gzxlo28Yq1uJ6FWw0fHVXJFEALla6Gol4ZC_c5DqpbciYoWwkK9HamkvHzma9u1fyIXqoPdXD_4dsHQN_Wlva_Q7O5br6jqzWSkX6rYoOP_jK0WnbjRWxnGmbGvtIpIA95hJ-m7n1Kudm5F3jXroJrHsUEpi2gMg1r6vJPkkR8rH_xT03VFucrKEjdjQDKdwk1lvocD1vhrr0DIudApn78I-BcieKc0yqnwYWNZCMMqCKF4mX42mC8GWdt-hcGlnzg4hOHJ_LZ03NWxb11CSGk13hv8qjDesBPfeZPFOBRw3cY1xHeu5BGlQg5V85Du8M2KZvlYN_DGp17A5LYtGWv-kHuytteGi_EKIIyADjoKke8o7SgmRYg1epb_ruqOLHsj4Mh81qF2c0deq-oAB2Db0Ncd9gSzblXRaIFUphechuxXON49WTKWP2E-rtZmxdPNO1NiucOfdlOf4q0HZV809sE");
                return params;
            }
        };

        requestQueue.add(jsonObjectRequest);
    }


}
